# SvelteKit with Asciidoc Starter

This project is to demonstrate using [Asciidoctor.js](https://docs.asciidoctor.org/asciidoctor.js/latest/) as the content processor for SvelteKit content using the AsciiDoc source format.
This is similar to the functionality found with [MDsveX](https://github.com/pngwn/MDsveX) for Markdown source files.

## Site Code repository

The source code for this site is hosted on GitLab: https://gitlab.com/zivbk1/sveltekit-asciidoc

## Netlify Configuration

This site is automatically built from commits to 'main' branch.
The Netlify URL is https://asciidoc-sveltekit.netlify.app

## Editing with GitPod

To work with this project, follow the steps below:

1. Create an Issue in the Project that describes the changes to be made and assign it to yourself if you plan on working on it.
2. After creating the Issue, and on the Issue page, click the "Create merge request" button.
This will make a new branch automatically and take you to the Merge Request creation page.
Assign this Merge Request to yourself if you plan on merging it to the main branch.
3. From the newly created Merge Request, click on the new branch name at the top of the page.
You will see it in some text like the following example `<Your Name> requested to merge <issue#-issue-title> into main <time reference>`.
This link will take you to your new branch in the repository.
4. Now, from the context of your new branch, click the "GitPod" button to the right of the "Clone" button.
You will need to install the [GitPod browser extension](https://www.gitpod.io/docs/configure/user-settings/browser-extension) for this button to show up.
5. GitPod will open a full configured workspace for you that includes everything you will need to edit this site.
In addition to the editor tool, it will automatically open a live preview of the site that will refresh for you as you save changes to files in the project.
6. Edit all the files that you need to and save changes along the way, watching the site preview update as you work.
7. Make Commits of your changes in the Gitpod workspace and then Sync them up to your new branch in the GitLab project repository.
8. When all your commits have been made and pushed to your branch it is time to merge those changes into main.
Go to your Merge Request and from there you can review all the changes that have been made to the new branch.
Mark the branch as ready if it isn't already.
Click the options to Delete the Branch.  
If you have more than 1 commit in your branch, then click the option to Squash Commits.
9. See your changes have been merged into the main branch, your merge request has been merged, your task branch has been deleted, and the original issue has been closed.
10. Profit!
