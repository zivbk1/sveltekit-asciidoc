import Processor from '@asciidoctor/core'
const processor = Processor()

export const load = async ({ url }) => {
  let allPostFiles = import.meta.glob("./**/*.adoc");
  const iterablePostFiles = Object.entries(allPostFiles);
  let adocFile = undefined;
  let attributes = undefined;
  let allPosts = [];

  if (url.pathname != "/") {
    if (url.pathname.includes("/blog/")) {
      let splitPath = url.pathname.split('/')
      adocFile = await import(`./${splitPath[1]}/${splitPath[2]}.adoc?raw`)
    } else {
      let trimmedPath = url.pathname.slice(1)
      adocFile = await import(`./${trimmedPath}/+page.adoc?raw`)
    }
  }

  if (adocFile) {
    attributes = processor.load(adocFile.default).getAttributes()
  }

  allPosts = await Promise.all(
    iterablePostFiles.map(async ([path]) => {
      let postPath = "";
      if (path.includes("blog")) {
        postPath = path.slice(1, -5);
      } else {
        postPath = path.slice(2, -11);
      }
      return {
        path: postPath,
      };
    })
  );

  return {
    posts: allPosts,
    attributes
  }
};