import { error } from '@sveltejs/kit';
import Processor from '@asciidoctor/core'

const processor = Processor()

export const load = (async ({ params }) => {
  const adocFile = await import(`../${params.slug}.adoc?raw`)
  if (adocFile) {
    let attributes = processor.load(adocFile.default).getAttributes()
    let html = processor.convert(adocFile.default, { 'standalone': false, 'safe': 'unsafe', 'attributes': { 'icons': 'font', "allow-uri-read": '' } })
    return {
      html,
      attributes
    }
  }
  error(404, 'Not found');
});