import adapter from '@sveltejs/adapter-netlify';
import preprocess from 'svelte-preprocess'
import Processor from '@asciidoctor/core'
const processor = Processor()

function svasciidoc() {
  return {
    markup: async ({ content, filename }) => {
      if (filename.endsWith('adoc')) {
        const html = processor.convert(content, { 'standalone': false, 'safe': 'unsafe', 'attributes': { 'icons': 'font', "allow-uri-read": '' } })
        return {
          code: html
        };
      }
    }
  }
};

/** @type {import('@sveltejs/kit').Config} */
const config = {
  extensions: ['.svelte', '.adoc'],
  kit: {
    adapter: adapter()
  },
  preprocess: [
    preprocess(),
    svasciidoc()
  ]
};

export default config;