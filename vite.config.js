import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';

export default defineConfig({
	plugins: [
		sveltekit()
	],
	ignore: ['**/*.adoc'],
	server: {
		hmr: {
			clientPort: process.env.HMR_HOST ? 443 : 24678,
			host: process.env.HMR_HOST
				? process.env.HMR_HOST.substring("https://".length)
				: "localhost"
		}
	}
});
